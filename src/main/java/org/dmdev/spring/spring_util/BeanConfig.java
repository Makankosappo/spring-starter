package org.dmdev.spring.spring_util;

import org.dmdev.spring.AOP.Aspects.CompanyAspects;
import org.dmdev.spring.AOP.Employee;
import org.dmdev.spring.AOP.Company;
import org.dmdev.spring.spring_introduction.Dog;
import org.dmdev.spring.spring_introduction.Person;
import org.dmdev.spring.spring_introduction.Pet;
import org.springframework.context.annotation.*;

import java.util.ArrayList;

@Configuration
@ComponentScan("org.dmdev.spring.AOP.Aspects")
@EnableAspectJAutoProxy
public class BeanConfig {


    @Bean
    @Scope(scopeName = "prototype")
    public Company companyBean() {
        return Company.builder()
                .name("Google")
                .employee(new ArrayList<>())
                .build();
    }

    @Bean
    @Scope(scopeName = "prototype")
    public Employee employeeBean() {
        return Employee.builder()
                .age(19)
                .firstname("Dmytro")
                .lastname("Matvieiev")
                .salary(200)
                .company(companyBean())
                .build();
    }


}

package org.dmdev.spring.spring_util;

import lombok.experimental.UtilityClass;
import org.dmdev.spring.AOP.Company;
import org.dmdev.spring.AOP.Employee;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


@UtilityClass
public class HibernateUtil {

    public static SessionFactory buildSessionFactory(){
        Configuration configuration = buildConfiguration();
        configuration.configure();

        return configuration.buildSessionFactory();
    }

    public static Configuration buildConfiguration(){
        Configuration configuration = new Configuration();

        configuration.addAnnotatedClass(Company.class);
        configuration.addAnnotatedClass(Employee.class);

        return configuration;
    }
}

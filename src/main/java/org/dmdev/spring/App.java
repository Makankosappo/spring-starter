package org.dmdev.spring;

import jakarta.transaction.Transactional;
import org.dmdev.spring.AOP.Company;
import org.dmdev.spring.AOP.Employee;
import org.dmdev.spring.spring_util.BeanConfig;
import org.dmdev.spring.spring_util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

    @Transactional
    public static void main(String[] args) {


        try (SessionFactory sessionFactory = HibernateUtil.buildSessionFactory();
             Session session = sessionFactory.openSession();
             AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(BeanConfig.class)) {

            session.beginTransaction();



            session.getTransaction().commit();

        }

    }
}


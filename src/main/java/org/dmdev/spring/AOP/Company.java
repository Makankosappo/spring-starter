package org.dmdev.spring.AOP;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 128, nullable = false)
    private String name;

    @OneToMany(mappedBy = "company")
    private List<Employee> employee = new ArrayList<>();

    public void addEmployee(Employee e) {
        this.employee.add(e);
        e.setCompany(this);
    }

    public Employee getEmployee(int index){
        if (checkIndex(index))
            return employee.get(index);
        else throw new IndexOutOfBoundsException();
    }

    private boolean checkIndex(int index){
        return this.employee.size() < index || 0 >= index;
    }


}

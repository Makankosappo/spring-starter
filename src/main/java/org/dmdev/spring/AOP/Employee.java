package org.dmdev.spring.AOP;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dmdev.spring.AOP.Company;
import org.springframework.lang.Nullable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 64, nullable = false)
    private String firstname;

    @Column(length = 64, nullable = false)
    private String lastname;

    private int age;

    private int salary;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    @Override
    public String toString(){
        return "<Employee info:>"
                +"\n * firstname: " + firstname
                +"\n * lastname: " + lastname
                +"\n * age: " + age
                +"\n * salary: " + salary;
    }

}

package org.dmdev.spring.AOP.Aspects;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.dmdev.spring.AOP.Company;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Component
@Aspect
public class CompanyAspects{

    private final Logger logger = LoggerFactory.getLogger(Company.class);

    @Pointcut("execution(* org.dmdev.spring.AOP.Company.get* (..))")
    protected void allGetMethods(){}

    @Pointcut("execution(* org.dmdev.spring.AOP.Company.add* (..))")
    protected void allAddMethods(){}

    @Pointcut("execution(* org.dmdev.spring.AOP.Company.set* (..))")
    protected void allSetMethods(){}

    @Pointcut("execution(* org.dmdev.spring.AOP.Company.* (..))")
    private void allCompanyMethods(){}


    @Before("allGetMethods()")
    public void beforeGetMethodsAdvice(JoinPoint joinPoint){
        Method method = ((MethodSignature)joinPoint.getSignature()).getMethod();
        logger.info("Attempt to get {} in method {}", method.getReturnType(), method.getName());
    }

    @Before("allAddMethods()")
    public void beforeAddMethodsAdvice(JoinPoint joinPoint){
        Method method = ((MethodSignature)joinPoint.getSignature()).getMethod();
        logger.info("Attempt to add {} in method {}", method.getParameterTypes(), method.getName());
    }

    @Before("allSetMethods()")
    public void beforeSetMethodsAdvice(JoinPoint joinPoint){
        Method method = ((MethodSignature)joinPoint.getSignature()).getMethod();
        logger.info("Attempt to set {} in method {}", method.getParameterTypes(), method.getName());
    }

    @AfterReturning(pointcut = "allGetMethods()")
    public void afterReturningAdvice(JoinPoint joinPoint){
        Method method = ((MethodSignature)joinPoint.getSignature()).getMethod();
        logger.info("Object {} was returned", method.getReturnType());
    }

    @AfterReturning("allAddMethods()")
    public void afterAddingAdvice(JoinPoint joinPoint){
        Method method = ((MethodSignature)joinPoint.getSignature()).getMethod();
        logger.info("Object {} was added", (Object[])method.getParameterTypes());
    }

    @AfterThrowing(pointcut = "allCompanyMethods()", throwing = "e")
    public void afterThrowingCompanyMethodsAdvice(JoinPoint joinPoint, Throwable e){
        Method method = ((MethodSignature)joinPoint.getSignature()).getMethod();
        logger.error("Exception in {} with cause = {}: {}", method.getName(), e != null ? e.getClass().getName() : "NULL", e != null && e.getMessage() != null ? e.getMessage() : "");
    }


}

package org.dmdev.spring.spring_introduction;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/*@Component("Dog")
@Scope("prototype")*/
public class Dog implements Pet {

    @Override
    public void voice() {
        System.out.println("Bark");
    }

//    @PostConstruct
    public void init() {
        System.out.println("Dog.Class : init method");
    }

//    @PreDestroy
    public void destroy() {
        System.out.println("Dog.Class : destroy method");
    }

}

package org.dmdev.spring.spring_introduction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

//@Component("personBean")
public class Person {

    private Pet pet;
//    @Value("${person.lastname}")
    private String lastname;

//    @Value("${person.age}")
    private int age;


//    @Autowired
    public Person(/*@Qualifier("Dog")*/ Pet pet) {
        this.pet = pet;
    }

/*    public Person(){
    }*/

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void callPet() {
        System.out.println("Pet, voice");
        pet.voice();
    }

    public String toString() {
        String info = "<Info>: " + "\n * Lastname: " + lastname +
                "\n * Age: " + age + "\n";
        return info;
    }
}
